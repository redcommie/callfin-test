<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class AjaxController extends Controller
{
    public function addBook(Request $request)
    {
        $data = $request->all();
        $book = new Book();

        try{
            $book->fill($data);
            $book->save();
        }
        catch(\Exception $e){
            echo $e->getMessage();
        }
        return json_encode($book, JSON_UNESCAPED_UNICODE);
    }


    public function deleteBook($id)
    {
        $book = Book::find($id);
        $path = public_path().'\upload\\'.$book->img;

        File::delete($path);
        Book::destroy($id);

        return json_encode($path, JSON_UNESCAPED_UNICODE);
    }

}
