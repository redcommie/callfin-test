@extends('layouts.site')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Книги в наличии:</h1>
            </div>
            <div class="books-block">
                @foreach($books as $book)

                    <div class="col-md-4 book">

                        <div class="book-cover-block">
                            <div style="background: url({{asset('/upload/'.$book->img)}}">
                            </div>
                            <p>Обложка отсутствует</p>
                        </div>
                        <p class="book-id" data-id="{{ $book->id }}">ID: {{ $book->id }}</p>
                        <h2>{{ $book->title }}</h2>
                        <p><b>Автор: </b>{{ $book->author }}</p>
                        <p><b>Издательство: </b>{{ $book->publish }}</p>
                        <p><b>Дата публикации: </b>{{ $book->date }}</p>
                        <form action="{{ route('addImage', $book->id) }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group cover-form">
                                <label for="img">Выберите обложку:</label>
                                <input id="img" type="file" name="file">
                            </div>
                            <button type="submit" class="btn btn-default">Добавить</button>
                        </form>

                        <button class="delete-book-btn btn btn-danger">Delete</button>

                    </div>

                @endforeach
            </div>
        </div>

        <hr>


    </div>

@endsection